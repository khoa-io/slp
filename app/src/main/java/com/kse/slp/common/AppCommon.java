package com.kse.slp.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides public singleton objects.
 *
 * @author Hoàng Văn Khoa
 * @since 03-09-2016.
 */
public class AppCommon {
    public static final String TAG = "SLP_LOG";

    public static final Logger LOGGER = LoggerFactory.getLogger(TAG);
    public static final Gson GSON = (new GsonBuilder()).create();
}
