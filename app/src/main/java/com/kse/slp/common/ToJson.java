package com.kse.slp.common;

import org.json.JSONObject;

/**
 * @author Hoàng Văn Khoa
 * @since 05-09-2016.
 */
public interface ToJson {
    JSONObject toJson();
}
