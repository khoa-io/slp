package com.kse.slp.common;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;

import com.kse.slp.R;

import java.util.Random;

/**
 * Cung cấp các {@link Dialog} thường dùng như:
 * <ul>
 * <li>Thông tin thêm</li>
 * <li>Thông tin cảnh báo</li>
 * <li>Câu hỏi Có/Không</li>
 * <li>Thông báo lỗi</li>
 * <li>Thông báo đang xử lí</li>
 * </ul>
 * <p>
 * Cách sử dụng: Sử dụng lớp {@link CommonDialog.Builder} để tạo ra instance phù hợp rồi gọi
 * {@link CommonDialog#show(FragmentManager)} hoặc {@link CommonDialog#show(FragmentTransaction)}.
 *
 * @author Hoàng Văn Khoa
 * @since 24-09-2016.
 */
public class CommonDialog extends DialogFragment {
    public static final int TYPE_QUESTION = 0;
    public static final int TYPE_INFO = 1;
    public static final int TYPE_WARN = 2;
    public static final int TYPE_ERROR = 3;
    public static final int TYPE_PROGRESS = 4;

    protected static final String ARG_PARAM_TYPE = "CommonDialog.TYPE";
    protected static final String ARG_PARAM_TITLE = "CommonDialog.TITLE";
    protected static final String ARG_PARAM_MESSAGE = "CommonDialog.MESSAGE";
    protected static final String ARG_PARAM_INDETERMINATE = "CommonDialog.INDETERMINATE";
    protected static final String ARG_PARAM_TAG = "CommonDialog.TAG";
    protected static final String ARG_PARAM_DATA = "CommonDialog.DATA";

    protected static final String ARG_PARAM_POSITIVE_BUTTON_TEXT = "CommonDialog.POSITIVE_BUTTON_TEXT";
    protected static final String ARG_PARAM_NEGATIVE_BUTTON_TEXT = "CommonDialog.NEGATIVE_BUTTON_TEXT";
    protected static final String ARG_PARAM_NEUTRAL_BUTTON_TEXT = "CommonDialog.NEUTRAL_BUTTON_TEXT";

    private int type;
    private String title;
    private String message;
    private String tag;
    /**
     * Data in JSON format {@link String}
     */
    private String data;
    private boolean indeterminate = false;
    private String positiveButtonText;
    private String negativeButtonText;
    private String neutralButtonText;

    private DialogInterface.OnClickListener onClickPositiveButton;
    private DialogInterface.OnClickListener onClickNegativeButton;
    private DialogInterface.OnClickListener onClickNeutralButton;
    private DialogInterface.OnDismissListener onDismiss;

    public void setType(int type) {
        this.type = type;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setRandomTag(String tag) {
        this.tag = tag;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setIndeterminate(boolean indeterminate) {
        this.indeterminate = indeterminate;
    }

    public CommonDialog() {
        // Required empty public constructor
    }

    public static CommonDialog.Builder getBuilder() {
        return new Builder();
    }


    public void setPositiveButton(String positiveButtonText, DialogInterface.OnClickListener onClickPositiveButton) {
        this.positiveButtonText = positiveButtonText;
        this.onClickPositiveButton = onClickPositiveButton;
    }

    public void setNegativeButton(String negativeButtonText, DialogInterface.OnClickListener onClickNegativeButton) {
        this.negativeButtonText = negativeButtonText;
        this.onClickNegativeButton = onClickNegativeButton;
    }

    public void setNeutralButton(String neutralButtonText, DialogInterface.OnClickListener onClickNeutralButton) {
        this.neutralButtonText = neutralButtonText;
        this.onClickNeutralButton = onClickNeutralButton;
    }

    public void setOnDismiss(DialogInterface.OnDismissListener onDismiss) {
        this.onDismiss = onDismiss;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args == null) throw new RuntimeException("Dialog has no args!");

        type = args.getInt(ARG_PARAM_TYPE);
        title = args.getString(ARG_PARAM_TITLE);
        message = args.getString(ARG_PARAM_MESSAGE);

        tag = args.getString(ARG_PARAM_TAG);
        data = args.getString(ARG_PARAM_DATA);

        indeterminate = args.getBoolean(ARG_PARAM_INDETERMINATE);

        positiveButtonText = args.getString(ARG_PARAM_POSITIVE_BUTTON_TEXT);
        negativeButtonText = args.getString(ARG_PARAM_NEGATIVE_BUTTON_TEXT);
        neutralButtonText = args.getString(ARG_PARAM_NEUTRAL_BUTTON_TEXT);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(message);

        switch (type) {
            case TYPE_PROGRESS:
                ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setIndeterminate(indeterminate);
                progressDialog.setMessage(message);
                return progressDialog;

            case TYPE_QUESTION:
                builder.setIcon(R.drawable.ic_help_white_48dp);
                if (onClickPositiveButton != null) {
                    builder.setPositiveButton(positiveButtonText, onClickPositiveButton);
                }
                if (onClickNegativeButton != null) {
                    builder.setNegativeButton(negativeButtonText, onClickNegativeButton);
                }
                if (onClickNeutralButton != null) {
                    builder.setNeutralButton(neutralButtonText, onClickNeutralButton);
                }
                break;

            case TYPE_INFO:
                builder.setIcon(R.drawable.ic_info_white_48dp);
                if (onClickPositiveButton != null) {
                    builder.setPositiveButton(positiveButtonText, onClickPositiveButton);
                }
                break;

            case TYPE_WARN:
                builder.setIcon(R.drawable.ic_warning_white_48dp);
                if (onClickPositiveButton != null) {
                    builder.setPositiveButton(positiveButtonText, onClickPositiveButton);
                }
                break;

            case TYPE_ERROR:
                builder.setIcon(R.drawable.ic_report_white_48dp);
                if (onClickPositiveButton != null) {
                    builder.setPositiveButton(positiveButtonText, onClickPositiveButton);
                }
                break;
        }

        return builder.create();
    }

    /**
     * Set {@link CommonDialog#tag} if it isn't set.
     */
    private void setRandomTag() {
        if (tag == null || tag.isEmpty()) {
            Random r = new Random();
            tag = String.valueOf(r.nextInt());
        }
    }

    public void show(FragmentManager manager) {
        setRandomTag();
        show(manager, tag);
    }

    public void show(FragmentTransaction transaction) {
        setRandomTag();
        show(transaction, tag);
    }

    public static class Builder {
        private int type;
        private String title;
        private String message;
        private boolean indeterminate;
        private String tag;
        private String data;

        private String positiveButtonText;
        private String negativeButtonText;
        private String neutralButtonText;

        private DialogInterface.OnClickListener onClickPositiveButton;
        private DialogInterface.OnClickListener onClickNegativeButton;
        private DialogInterface.OnClickListener onClickNeutralButton;
        private DialogInterface.OnDismissListener onDismiss;

        /**
         * @param type One of {@link CommonDialog#TYPE_ERROR},
         *             {@link CommonDialog#TYPE_INFO},
         *             {@link CommonDialog#TYPE_QUESTION},
         *             {@link CommonDialog#TYPE_PROGRESS},
         *             {@link CommonDialog#TYPE_WARN}.
         */
        public Builder setType(int type) {
            this.type = type;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setIndeterminate(boolean indeterminate) {
            this.indeterminate = indeterminate;
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText, DialogInterface.OnClickListener onClickPositiveButton) {
            this.positiveButtonText = positiveButtonText;
            this.onClickPositiveButton = onClickPositiveButton;
            return this;
        }

        public Builder setNegativeButton(String negativeButtonText, DialogInterface.OnClickListener onClickNegativeButton) {
            this.negativeButtonText = negativeButtonText;
            this.onClickNegativeButton = onClickNegativeButton;
            return this;
        }

        public Builder setNeutralButton(String neutralButtonText, DialogInterface.OnClickListener onClickNeutralButton) {
            this.neutralButtonText = neutralButtonText;
            this.onClickNeutralButton = onClickNeutralButton;
            return this;
        }

        public DialogInterface.OnDismissListener getOnDismiss() {
            return onDismiss;
        }

        public Builder setOnDismiss(DialogInterface.OnDismissListener onDismiss) {
            this.onDismiss = onDismiss;
            return this;
        }

        public Builder setTag(String tag) {
            this.tag = tag;
            return this;
        }

        public Builder setData(String data) {
            this.data = data;
            return this;
        }

        public CommonDialog create() {
            CommonDialog dialog = new CommonDialog();

            Bundle args = new Bundle();

            args.putInt(ARG_PARAM_TYPE, type);
            args.putString(ARG_PARAM_MESSAGE, message);
            args.putString(ARG_PARAM_TITLE, title);

            args.putString(ARG_PARAM_TAG, tag);
            args.putString(ARG_PARAM_DATA, data);

            args.putBoolean(ARG_PARAM_INDETERMINATE, indeterminate);

            args.putString(ARG_PARAM_POSITIVE_BUTTON_TEXT, positiveButtonText);
            args.putString(ARG_PARAM_NEGATIVE_BUTTON_TEXT, negativeButtonText);
            args.putString(ARG_PARAM_NEUTRAL_BUTTON_TEXT, neutralButtonText);

            dialog.setArguments(args);

            dialog.setPositiveButton(positiveButtonText, onClickPositiveButton);
            dialog.setNegativeButton(negativeButtonText, onClickNegativeButton);
            dialog.setNeutralButton(neutralButtonText, onClickNeutralButton);

            dialog.setOnDismiss(onDismiss);

            return dialog;
        }

        /**
         * Create and show
         */
        public void show(FragmentManager fm) {
            create().show(fm);
        }

        /**
         * Create and show
         */
        public void show(FragmentTransaction ft) {
            create().show(ft);
        }

    }
}
