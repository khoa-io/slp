package com.kse.slp;

import android.app.Application;

import java.net.CookieHandler;
import java.net.CookieManager;

/**
 * @author Hoàng Văn Khoa
 * @since 04-09-2016.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        CookieHandler.setDefault(new CookieManager());
    }

}
