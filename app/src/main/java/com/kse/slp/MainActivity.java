package com.kse.slp;

import android.app.ActivityManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.kse.slp.auth.AuthenticationInfo;
import com.kse.slp.auth.LoginDialogFragment;
import com.kse.slp.client.Client;
import com.kse.slp.client.ResponseListener;
import com.kse.slp.client.ResultInfo;
import com.kse.slp.common.AppCommon;
import com.kse.slp.common.CommonDialog;
import com.kse.slp.route.MapsActivity;
import com.kse.slp.route.Point;
import com.kse.slp.route.RouteInfo;
import com.kse.slp.ship.LocationService;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        LoginDialogFragment.SignInDialogListener {

    private static final int REQUEST_CODE_RESTART_MAP = 1;

    private Client client;

    private ResponseListener<ResultInfo> authListener;

    private LoginDialogFragment loginDialog;
    private CommonDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        client = new Client(this);

        progressDialog = CommonDialog.getBuilder()
                .setType(CommonDialog.TYPE_PROGRESS)
                .setMessage(getString(R.string.message_wait))
                .create();

        authListener = new AuthenticationListener();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        if (drawer != null) {
            drawer.addDrawerListener(toggle);
        }
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!client.isAuthenticated()) {
            FragmentManager fm = getSupportFragmentManager();
            loginDialog = LoginDialogFragment.newInstance();
            loginDialog.show(fm, "dialog_fragment_sign_in");
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer == null) return;

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return false;
            case R.id.action_track_location:
                if (isTrackingLocation()) {
                    CommonDialog.getBuilder()
                            .setType(CommonDialog.TYPE_WARN)
                            .setMessage(getString(R.string.warn_tracking))
                            .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    item.setTitle(R.string.action_stop_track_location);
                                }
                            })
                            .create().show(getSupportFragmentManager());
                    return true;
                }
                Intent locationService = new Intent(this, LocationService.class);
                locationService.setAction(getString(R.string.package_name) + getString(R.string.app_action_track_location));
                startService(locationService);

                Toast.makeText(this, R.string.info_tracking_location, Toast.LENGTH_SHORT).show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isTrackingLocation() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (service.service.getClassName().equals(LocationService.class.getName())) {
                return true;
            }
        }
        return false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_view_route) {
            progressDialog.show(getSupportFragmentManager());
            client.getRoute(new RouteInfoResponseListener());
        } else if (id == R.id.nav_manage) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        client.cancel();
    }

    @Override
    public void onClickSignIn(final String username, final String password, boolean rememberLogin) {
        client.login(new AuthenticationInfo(username, password), authListener);
        progressDialog.show(getSupportFragmentManager());
        Toast.makeText(MainActivity.this, "Logging in", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickCancel() {
        finish();
    }

    private class OnClickRoute implements DialogInterface.OnClickListener {
        private String[] items;
        private List<RouteInfo> routeInfos;

        public OnClickRoute(String[] items, List<RouteInfo> routeInfos) {
            this.items = items;
            this.routeInfos = routeInfos;
        }

        public void onClick(DialogInterface dialog, final int which) {
            AppCommon.LOGGER.info(items[which]);
            progressDialog.show(getSupportFragmentManager());

            viewMap(routeInfos.get(which));
        }
    }

    private void viewMap(final RouteInfo routeInfo) {
        client.getRouteDetail(routeInfo, new ResponseListener<List<Point>>() {
            @Override
            public void onResponse(List<Point> data) {
                progressDialog.dismiss();
                Intent map = new Intent(MainActivity.this, MapsActivity.class);
                map.setAction(getString(R.string.package_name) + getString(R.string.app_action_view_route));
                Bundle extras = new Bundle();

                extras.putString(
                        getString(R.string.param_point_list),
                        AppCommon.GSON.toJson(data.toArray())
                );

                extras.putString(
                        getString(R.string.param_route_info),
                        AppCommon.GSON.toJson(routeInfo)
                );

                map.putExtras(extras);
                startActivityForResult(map, REQUEST_CODE_RESTART_MAP);
            }

            @Override
            public void onError(Exception e) {
                progressDialog.dismiss();
                CommonDialog.getBuilder()
                        .setType(CommonDialog.TYPE_ERROR)
                        .setMessage(e.getMessage())
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .create().show(getSupportFragmentManager());
            }

            @Override
            public void onClientError(HttpClientErrorException e) {
                progressDialog.dismiss();
                CommonDialog.getBuilder()
                        .setType(CommonDialog.TYPE_ERROR)
                        .setMessage(e.getMessage())
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .create().show(getSupportFragmentManager());
            }

            @Override
            public void onServerError(HttpServerErrorException e) {
                progressDialog.dismiss();
                CommonDialog.getBuilder()
                        .setType(CommonDialog.TYPE_ERROR)
                        .setMessage(e.getMessage())
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .create().show(getSupportFragmentManager());
            }
        });
    }

    private class AuthenticationListener implements ResponseListener<ResultInfo> {

        @Override
        public void onResponse(ResultInfo resultInfo) {
            if (resultInfo.getResult() == ResultInfo.RESULT_SUCCESS) {
                // Success
                CommonDialog.getBuilder()
                        .setType(CommonDialog.TYPE_INFO)
                        .setMessage(getString(R.string.auth_success))
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                progressDialog.dismiss();
                                loginDialog.dismiss();
                            }
                        })
                        .create()
                        .show(getSupportFragmentManager());
                return;
            }

            // Error
            CommonDialog.getBuilder()
                    .setType(CommonDialog.TYPE_ERROR)
                    .setMessage(getString(R.string.auth_failed))
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            progressDialog.dismiss();
                            finish();
                        }
                    })
                    .create()
                    .show(getSupportFragmentManager());
        }

        @Override
        public void onError(Exception e) {
            CommonDialog.getBuilder()
                    .setType(CommonDialog.TYPE_ERROR)
                    .setTitle(getString(R.string.auth_failed))
                    .setMessage(e.getMessage())
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            progressDialog.dismiss();
                            finish();
                        }
                    })
                    .create()
                    .show(getSupportFragmentManager());
        }

        @Override
        public void onClientError(HttpClientErrorException e) {
            CommonDialog.getBuilder()
                    .setType(CommonDialog.TYPE_ERROR)
                    .setTitle(getString(R.string.auth_failed))
                    .setMessage(e.getMessage())
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            progressDialog.dismiss();
                            finish();
                        }
                    })
                    .create()
                    .show(getSupportFragmentManager());
        }

        @Override
        public void onServerError(HttpServerErrorException e) {
            CommonDialog.getBuilder()
                    .setType(CommonDialog.TYPE_ERROR)
                    .setTitle(getString(R.string.auth_failed))
                    .setMessage(e.getMessage())
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            progressDialog.dismiss();
                            finish();
                        }
                    })
                    .create()
                    .show(getSupportFragmentManager());
        }
    }

    private class RouteInfoResponseListener implements ResponseListener<List<RouteInfo>> {

        @Override
        public void onResponse(List<RouteInfo> routeInfos) {
            progressDialog.dismiss();
            if (routeInfos.isEmpty()) {
                CommonDialog.getBuilder()
                        .setType(CommonDialog.TYPE_WARN)
                        .setMessage(getString(R.string.empty_route))
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .create().show(getSupportFragmentManager());
            }

            String[] items = new String[routeInfos.size()];

            for (int i = 0; i < items.length; ++i) {
                items[i] = routeInfos.get(i).getCode();
            }

            new AlertDialog.Builder(MainActivity.this)
                    .setTitle(R.string.pick_route)
                    .setItems(items, new OnClickRoute(items, routeInfos))
                    .show();
        }

        @Override
        public void onError(Exception e) {
            progressDialog.dismiss();

        }

        @Override
        public void onClientError(HttpClientErrorException e) {
            progressDialog.dismiss();

        }

        @Override
        public void onServerError(HttpServerErrorException e) {
            progressDialog.dismiss();

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode != REQUEST_CODE_RESTART_MAP || resultCode != RESULT_OK) {
            return;
        }

        String rawRouteInfo = data.getExtras().getString(getString(R.string.param_route_info));
        RouteInfo routeInfo = AppCommon.GSON.fromJson(rawRouteInfo, RouteInfo.class);

        viewMap(routeInfo);
    }
}
