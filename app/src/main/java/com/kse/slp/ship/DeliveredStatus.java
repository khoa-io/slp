package com.kse.slp.ship;

/**
 * @author Hoàng Văn Khoa
 * @since 08-10-2016.
 */
public class DeliveredStatus {
    public static final String ORDER_STATUS_NOT_IN_ROUTE = "NOT_IN_ROUTE";
    public static final String ORDER_STATUS_IN_ROUTE = "IN_ROUTE";
    public static final String ORDER_STATUS_DELIVERED = "DELIVERIED";
    public static final String ORDER_STATUS_ARRIVED_BUT_NOT_DELIVERED = "ARRIVED_BUT_NOT_DELIVERIED";
    public static final String ROUTE_STATUS_UNDER_CREATION = "UNDER_CREATION";
    public static final String ROUTE_STATUS_CONFIRMED = "CONFIRMED";

    private String orderCode;
    private String status;

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
