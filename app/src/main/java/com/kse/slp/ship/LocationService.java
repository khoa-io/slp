package com.kse.slp.ship;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.kse.slp.R;
import com.kse.slp.RequestPermissionActivity;
import com.kse.slp.client.Client;
import com.kse.slp.client.ResponseListener;
import com.kse.slp.client.ResultInfo;
import com.kse.slp.common.AppCommon;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.Date;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, ResponseListener<ResultInfo> {

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;

    private Client client;

    public LocationService() {
        locationRequest = new LocationRequest();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        final String action_track_location = getString(R.string.package_name) + getString(R.string.app_action_track_location);

        if (!action_track_location.equals(action)) {
            return super.onStartCommand(intent, flags, startId);
        }

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (client == null) {
            client = new Client(this);
        }

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        googleApiClient.connect();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        stopLocationUpdates();
        googleApiClient.disconnect();
        super.onDestroy();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            AppCommon.LOGGER.error("App has no permissions!");
            return;
        }
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (lastLocation != null) {
            AppCommon.LOGGER.info("Update last location.");
            updateLocation(lastLocation);
        }

        AppCommon.LOGGER.info("onConnected: Starting tracking location.");
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        AppCommon.LOGGER.info("onConnectionSuspended: " + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppCommon.LOGGER.error("onConnectionFailed: " + connectionResult.getErrorMessage());
    }


    protected void startLocationUpdates() {
        boolean hasAccessFinePermission = ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED;

        boolean hasAccessCoarsePermission = ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED;

        boolean hasPermission = hasAccessFinePermission && hasAccessCoarsePermission;
        ;
        if (!hasPermission) {
            AppCommon.LOGGER.error("App has no permissions.");
            Intent requestPermissionActivity = new Intent(this, RequestPermissionActivity.class);
            requestPermissionActivity.setAction(getString(R.string.package_name) + getString(R.string.app_action_request_permission));
            requestPermissionActivity.setFlags(FLAG_ACTIVITY_NEW_TASK);

            startActivity(requestPermissionActivity);
            stopSelf();
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient, locationRequest, this
        );
        AppCommon.LOGGER.info("Started location tracking.");
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                googleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        AppCommon.LOGGER.info("onLocationChanged: " + location);
        updateLocation(location);
    }

    private void updateLocation(Location location) {
        LocationInfo locationInfo = new LocationInfo();
        locationInfo.setLatitude(location.getLatitude());
        locationInfo.setLongitude(location.getLongitude());
        locationInfo.setTimestamp(new Date().getTime());
        client.updateLocation(locationInfo, this);
    }

    @Override
    public void onResponse(ResultInfo data) {
        AppCommon.LOGGER.info("LocationUpdate: " + data.toString());
    }

    @Override
    public void onError(Exception e) {
        AppCommon.LOGGER.error("LocationUpdate", e);
    }

    @Override
    public void onClientError(HttpClientErrorException e) {
        AppCommon.LOGGER.error("LocationUpdate", e);
    }

    @Override
    public void onServerError(HttpServerErrorException e) {
        AppCommon.LOGGER.error("LocationUpdate", e);
    }
}
