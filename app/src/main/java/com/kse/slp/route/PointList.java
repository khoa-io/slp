package com.kse.slp.route;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Hoàng Văn Khoa
 * @since 11-09-2016.
 */
public class PointList {

    @SerializedName("route")
    private Point[] points;

    public List<Point> getPoints() {
        List<Point> points = new ArrayList<>();
        Collections.addAll(points, this.points);
        return points;
    }

    public void setPoints(Point[] points) {
        this.points = points;
    }
}
