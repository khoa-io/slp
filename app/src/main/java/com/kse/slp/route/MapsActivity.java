package com.kse.slp.route;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kse.slp.MainActivity;
import com.kse.slp.R;
import com.kse.slp.client.Client;
import com.kse.slp.client.ResponseListener;
import com.kse.slp.common.AppCommon;
import com.kse.slp.ship.DeliveredStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppCommon.TAG);

    private GoogleMap map;
    private List<Point> points;
    private Client client;
    private RouteInfo routeInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        client = new Client(this);

        final String action_view_route = getString(R.string.package_name) + getString(R.string.app_action_view_route);

        if (!action_view_route.equals(getIntent().getAction())) {
            finish();
            return;
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        String rawData = getIntent().getExtras().getString(getString(R.string.param_point_list));
        String rawRouteInfo = getIntent().getExtras().getString(getString(R.string.param_route_info));

        routeInfo = AppCommon.GSON.fromJson(rawRouteInfo, RouteInfo.class);

        points = new ArrayList<>();
        Collections.addAll(points, AppCommon.GSON.fromJson(rawData, Point[].class));
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        final float[] markerColors = new float[]{
                BitmapDescriptorFactory.HUE_RED,
                BitmapDescriptorFactory.HUE_GREEN,
                BitmapDescriptorFactory.HUE_YELLOW
        };

        map = googleMap;

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                AlertDialog dialog = new AlertDialog.Builder(MapsActivity.this)
                        .setTitle(marker.getTitle())
                        .setMessage(getString(R.string.dialog_delivery_status_question))
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int i) {
                                final Point point = (Point) marker.getTag();
                                DeliveredStatus status = new DeliveredStatus();
                                status.setOrderCode(point.getOrderCode());
                                status.setStatus(DeliveredStatus.ORDER_STATUS_DELIVERED);

                                client.updateDeliveredStatus(status, new ResponseListener<Void>() {
                                    @Override
                                    public void onResponse(Void data) {
                                        dialog.dismiss();

                                        Intent result = new Intent(MapsActivity.this, MainActivity.class);
                                        Bundle extras = new Bundle();

                                        extras.putString(
                                                getString(R.string.param_route_info),
                                                AppCommon.GSON.toJson(routeInfo)
                                        );
                                        result.putExtras(extras);

                                        setResult(RESULT_OK, result);
                                        finish();
                                    }

                                    @Override
                                    public void onError(Exception e) {
                                    }

                                    @Override
                                    public void onClientError(HttpClientErrorException e) {
                                        new AlertDialog.Builder(MapsActivity.this)
                                                .setTitle(R.string.title_error)
                                                .setIcon(R.drawable.ic_error_white_48dp)
                                                .setMessage(e.getMessage())
                                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                })
                                                .show();
                                    }

                                    @Override
                                    public void onServerError(HttpServerErrorException e) {
                                        new AlertDialog.Builder(MapsActivity.this)
                                                .setTitle(R.string.title_error)
                                                .setIcon(R.drawable.ic_error_white_48dp)
                                                .setMessage(e.getMessage())
                                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                })
                                                .show();
                                    }
                                });
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                dialog.dismiss();
                            }
                        })
                        .create();

                dialog.show();
                return true;
            }
        });

        if (points.size() <= 0) {
            new AlertDialog.Builder(MapsActivity.this)
                    .setIcon(R.drawable.ic_error_white_48dp)
                    .setMessage(R.string.empty_route)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();

            return;
        }

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        PolylineOptions rectOptions = new PolylineOptions();
        rectOptions.color(Color.GRAY);
        PolylineOptions unvisitedRectOptions = new PolylineOptions();
        unvisitedRectOptions.color(Color.RED);

        for (int i = 0; i < points.size(); i++) {
            Point currentPoint = points.get(i);

            LatLng currentPosition = new LatLng(currentPoint.getLatitude(), currentPoint.getLongitude());
            Marker marker;
            try {
                marker = map.addMarker(
                        new MarkerOptions()
                                .position(currentPosition)
                                .title(currentPoint.getOrderCode())
                                .icon(BitmapDescriptorFactory.defaultMarker(markerColors[currentPoint.getDelivered()]))
                );
                marker.setTag(currentPoint);
            } catch (IndexOutOfBoundsException e) {
                throw new RuntimeException(e);
            }
            builder.include(marker.getPosition());
            rectOptions.add(currentPosition);

            Point nextPoint;
            try {
                nextPoint = points.get(i + 1);
            } catch (IndexOutOfBoundsException e) {
                if (currentPoint.getDelivered() == Point.UNVISITED) {
                    unvisitedRectOptions.add(currentPosition);
                }
                break;
            }

            if (nextPoint.getDelivered() == Point.UNVISITED) {
                unvisitedRectOptions.add(currentPosition);
            }
        }

        bounds = builder.build();

        map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));
            }
        });


        // Get back the mutable Polyline
        map.addPolyline(rectOptions);
        map.addPolyline(unvisitedRectOptions);
    }

    private LatLngBounds bounds;
}
