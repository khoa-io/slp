package com.kse.slp.route;

import com.google.gson.annotations.SerializedName;
import com.kse.slp.common.AppCommon;
import com.kse.slp.common.ToJson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Sample:
 * <pre><code>{
 * "route_Shipper_Code": "tuandatshipper",
 * "route_Start_DateTime": "10:30",
 * "route_Status_Code": "CONFIRMED",
 * "route_ID": 16,
 * "route_Code": "tuandatshipper20160809113900"
 * }</code></pre>
 *
 * @author Hoàng Văn Khoa
 * @since 04-09-2016.
 */
public class RouteInfo implements ToJson {

    public static final String CONFIRMED = "CONFIRMED";
    public static final String UNDER_CREATION = "UNDER_CREATION";

    @SerializedName("route_Shipper_Code")
    private String shipperCode;

    @SerializedName("route_Start_DateTime")
    private String date;

    @SerializedName("route_Status_Code")
    private String status;

    @SerializedName("route_ID")
    private int id;

    @SerializedName("route_Code")
    private String code;

    public String getShipperCode() {
        return shipperCode;
    }

    public void setShipperCode(String shipperCode) {
        this.shipperCode = shipperCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        switch (status) {
            case "CONFIRMED":
                this.status = CONFIRMED;
                break;
            case "UNDER_CREATION":
                this.status = UNDER_CREATION;
                break;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public JSONObject toJson() {
        try {
            return new JSONObject(AppCommon.GSON.toJson(new RouteCode(code)));
        } catch (JSONException e) {
            return null;
        }
    }

    public static class RouteInfos {

        @SerializedName("route_info")
        private RouteInfo[] routeInfos;
    }

    private static class RouteCode {
        @SerializedName("routeCode")
        private String code;

        private RouteCode(String code) {
            this.code = code;
        }
    }
}
