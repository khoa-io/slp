package com.kse.slp.route;

import com.google.gson.annotations.SerializedName;

/**
 * @author Hoàng Văn Khoa
 * @since 04-09-2016.
 */
public class ListRoute {

    @SerializedName("listRoute")
    private RouteInfo[] listRoute;

    public RouteInfo[] getListRoute() {
        return listRoute;
    }

    public void setListRoute(RouteInfo[] listRoute) {
        this.listRoute = listRoute;
    }
}
