package com.kse.slp.route;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

/**
 * @author Hoàng Văn Khoa
 * @since 11-09-2016.
 */
public class Point {

    /**
     * Đã giao thành công
     */
    public static final int DELIVERED = 1;
    /**
     * Đã đến nhưng chưa giao được
     */
    public static final int NOT_DELIVERED_YET = 2;
    /**
     * Chưa đến
     */
    public static final int UNVISITED = 0;

    @SerializedName("lat")
    private double latitude;
    @SerializedName("lng")
    private double longitude;
    @SerializedName("orderCode")
    private String orderCode;
    @SerializedName("deliveried")
    private int delivered;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public int getDelivered() {
        return delivered;
    }

    public void setDelivered(int delivered) {
        this.delivered = delivered;
    }

    @Override
    public String toString() {
        return new GsonBuilder().create().toJson(this);
    }
}
