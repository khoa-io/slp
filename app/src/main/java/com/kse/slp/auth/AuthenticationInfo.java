package com.kse.slp.auth;

import com.google.gson.annotations.SerializedName;
import com.kse.slp.common.AppCommon;
import com.kse.slp.common.ToJson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Hoàng Văn Khoa
 * @since 04-09-2016.
 */
public class AuthenticationInfo implements ToJson {

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    public AuthenticationInfo(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public JSONObject toJson() {
        try {
            return new JSONObject(AppCommon.GSON.toJson(this));
        } catch (JSONException e) {
            return null;
        }
    }
}
