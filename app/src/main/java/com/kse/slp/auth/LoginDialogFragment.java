package com.kse.slp.auth;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.kse.slp.BuildConfig;
import com.kse.slp.R;
import com.kse.slp.common.AppCommon;
import com.kse.slp.common.AppPreferences;

/**
 * A simple {@link DialogFragment} subclass provides signing in function.
 * Activities that contain this fragment must implement the
 * {@link SignInDialogListener} interface
 * to handle interaction events.
 * Use the {@link LoginDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginDialogFragment extends DialogFragment {
    private SignInDialogListener listener;
    private EditText usernameEdit;
    private EditText passwordEdit;
    private CheckBox checkRememberLogin;

    public LoginDialogFragment() {
        // Required empty public constructor
    }

    public static LoginDialogFragment newInstance() {
        LoginDialogFragment fragment = new LoginDialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            // ignore for now
//        }
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.dialog_sign_in, null);
        builder.setView(view);

        Button signInButton = (Button) view.findViewById(R.id.btn_sign_in);
        Button cancelButton = (Button) view.findViewById(R.id.btn_cancel);
        usernameEdit = (EditText) view.findViewById(R.id.edit_username);
        passwordEdit = (EditText) view.findViewById(R.id.edit_password);
        checkRememberLogin = (CheckBox) view.findViewById(R.id.check_remember_login);

        AppPreferences prefs = AppPreferences.getInstance(getActivity());
        usernameEdit.setText(prefs.getString(getActivity().getString(R.string.username), ""));
        passwordEdit.setText(prefs.getString(getActivity().getString(R.string.password), ""));

        if (BuildConfig.DEBUG) {
            usernameEdit.setText("dungpqshipper");
            passwordEdit.setText("12345678");
        }

        signInButton.setOnClickListener(new OnClickSignInListener());
        cancelButton.setOnClickListener(new OnClickCancelListener());

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_sign_in, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SignInDialogListener) {
            listener = (SignInDialogListener) context;
            return;
        }
        throw new RuntimeException(context.toString() + " must implement SignInDialogListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface SignInDialogListener {
        /**
         * Call when user click "Sign in" button.
         */
        void onClickSignIn(String username, String password, boolean rememberLogin);

        /**
         * Call when user click "Cancel" button.
         */
        void onClickCancel();
    }

    private class OnClickSignInListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            if (listener == null) {
                throw new RuntimeException(getActivity().toString()
                        + " must implement SignInDialogListener");
            }

            String username = usernameEdit.getText().toString();
            String password = passwordEdit.getText().toString();
            boolean rememberLogin = checkRememberLogin.isChecked();

            listener.onClickSignIn(username, password, rememberLogin);
        }
    }

    private class OnClickCancelListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            if (listener == null) {
                throw new RuntimeException(getActivity().toString()
                        + " must implement SignInDialogListener");
            }
            dismiss();
            listener.onClickCancel();
        }
    }
}
