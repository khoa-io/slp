package com.kse.slp.client;

import com.kse.slp.R;
import com.kse.slp.ship.LocationInfo;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.util.Collections;

/**
 * @author Hoàng Văn Khoa
 * @since 01-10-2016.
 */
class UpdateLocationTask extends ClientBaseTask<LocationInfo, Void, ResultInfo> {

    UpdateLocationTask(Client client, ResponseListener<ResultInfo> listener) {
        super(client, listener);
    }

    @Override
    protected ResultInfo doInBackground(LocationInfo... locationInfos) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        String url = context.getString(R.string.url_slp_base) + context.getString(R.string.url_update_location);
        HttpEntity<LocationInfo> request = new HttpEntity<>(locationInfos[0], requestHeaders);
        Class<ResultInfo> responseType = ResultInfo.class;

        try {
            ResponseEntity<ResultInfo> response = rest.postForEntity(url, request, responseType);
            return response.getBody();
        } catch (ResourceAccessException | HttpClientErrorException | HttpServerErrorException e) {
            this.e = e;
            return null;
        }
    }
}
