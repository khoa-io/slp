package com.kse.slp.client;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

/**
 * @author Hoàng Văn Khoa
 * @since 01-10-2016.
 */
public interface ResponseListener<T> {
    void onResponse(T data);

    void onError(Exception e);

    void onClientError(HttpClientErrorException e);

    void onServerError(HttpServerErrorException e);
}
