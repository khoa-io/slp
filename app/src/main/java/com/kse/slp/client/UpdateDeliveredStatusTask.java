package com.kse.slp.client;

import com.kse.slp.R;
import com.kse.slp.ship.DeliveredStatus;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.util.Collections;

/**
 * @author Hoàng Văn Khoa
 * @since 08-10-2016.
 */
class UpdateDeliveredStatusTask extends ClientBaseTask<DeliveredStatus, Void, Void> {
    UpdateDeliveredStatusTask(Client client, ResponseListener<Void> listener) {
        super(client, listener);
    }

    @Override
    protected Void doInBackground(DeliveredStatus... statuses) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        String url = context.getString(R.string.url_slp_base) + context.getString(R.string.url_set_order_delivered);
        HttpEntity<DeliveredStatus> request = new HttpEntity<>(statuses[0], requestHeaders);

        try {
            rest.postForEntity(url, request, String.class);
        } catch (ResourceAccessException | HttpClientErrorException | HttpServerErrorException e) {
            this.e = e;
            return null;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (result == null) {
            listener.onResponse(null);
            return;
        }

        if (e instanceof HttpClientErrorException) {
            listener.onClientError((HttpClientErrorException) e);
            return;
        }

        if (e instanceof HttpServerErrorException) {
            listener.onServerError((HttpServerErrorException) e);
            return;
        }

        listener.onError(e);
    }
}
