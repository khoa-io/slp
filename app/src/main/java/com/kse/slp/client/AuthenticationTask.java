package com.kse.slp.client;

import com.kse.slp.R;
import com.kse.slp.auth.AuthenticationInfo;
import com.kse.slp.common.AppPreferences;

import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.util.Collections;

/**
 * @author Hoàng Văn Khoa
 * @since 01-10-2016.
 */
class AuthenticationTask extends ClientBaseTask<Void, Void, ResultInfo> {

    private AuthenticationInfo authInfo;

    AuthenticationTask(Client client, AuthenticationInfo authInfo, ResponseListener<ResultInfo> responseListener) {
        super(client, responseListener);
        this.authInfo = authInfo;
    }

    @Override
    protected ResultInfo doInBackground(Void... voids) {
        String username = authInfo.getUsername();
        String password = authInfo.getPassword();
        HttpAuthentication authHeader = new HttpBasicAuthentication(username, password);
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAuthorization(authHeader);
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        String url = context.getString(R.string.url_slp_base) + context.getString(R.string.url_login_android);
        HttpEntity request = new HttpEntity<>(requestHeaders);
        Class<ResultInfo> responseType = ResultInfo.class;

        try {
            ResponseEntity<ResultInfo> response = rest.postForEntity(url, request, responseType);
            return response.getBody();
        } catch (ResourceAccessException | HttpClientErrorException | HttpServerErrorException e) {
            this.e = e;
            return null;
        }
    }

    @Override
    protected void onPostExecute(ResultInfo resultInfo) {
        if (resultInfo != null) {
            AppPreferences.getInstance(context)
                    .putBoolean(context.getString(R.string.pref_is_authenticated), true);
            listener.onResponse(resultInfo);
            return;
        }

        if (e instanceof HttpClientErrorException) {
            listener.onClientError((HttpClientErrorException) e);
            return;
        }

        if (e instanceof HttpServerErrorException) {
            listener.onServerError((HttpServerErrorException) e);
            return;
        }

        listener.onError(e);
    }
}

