package com.kse.slp.client;

import android.content.Context;
import android.os.AsyncTask;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * @author Hoàng Văn Khoa
 * @since 01-10-2016.
 */
public abstract class ClientBaseTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    protected ResponseListener<Result> listener;
    protected Exception e;

    protected RestTemplate rest;

    protected Context context;

    ClientBaseTask(Client client, ResponseListener<Result> listener) {
        this.listener = listener;

        context = client.getContext();
        rest = client.getRest();
    }

    @Override
    protected void onPostExecute(Result result) {
        if (result != null) {
            listener.onResponse(result);
            return;
        }

        if (e instanceof HttpClientErrorException) {
            listener.onClientError((HttpClientErrorException) e);
            return;
        }

        if (e instanceof HttpServerErrorException) {
            listener.onServerError((HttpServerErrorException) e);
            return;
        }

        listener.onError(e);
    }
}
