package com.kse.slp.client;

import com.google.gson.annotations.SerializedName;

/**
 * @author Hoàng Văn Khoa
 * @since 05-09-2016.
 */
public class ResultInfo {

    public static final int RESULT_SUCCESS = 0;

    @SerializedName("result")
    private int result;

    public ResultInfo(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
