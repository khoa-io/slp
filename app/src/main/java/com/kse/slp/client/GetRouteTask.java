package com.kse.slp.client;

import com.kse.slp.R;
import com.kse.slp.route.ListRoute;
import com.kse.slp.route.RouteInfo;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Hoàng Văn Khoa
 * @since 01-10-2016.
 */
class GetRouteTask extends ClientBaseTask<Void, Void, List<RouteInfo>> {

    GetRouteTask(Client client, ResponseListener<List<RouteInfo>> listener) {
        super(client, listener);
    }

    @Override
    protected List<RouteInfo> doInBackground(Void... voids) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        String url = context.getString(R.string.url_slp_base) + context.getString(R.string.url_get_route_android);
        HttpEntity request = new HttpEntity<>(requestHeaders);
        Class<ListRoute> responseType = ListRoute.class;

        try {
            List<RouteInfo> routeInfos = new ArrayList<>();
            ResponseEntity<ListRoute> response = rest.postForEntity(url, request, responseType);
            ListRoute listRoute = response.getBody();
            Collections.addAll(routeInfos, listRoute.getListRoute());
            return routeInfos;
        } catch (ResourceAccessException | HttpClientErrorException | HttpServerErrorException e) {
            this.e = e;
            return null;
        }
    }
}
