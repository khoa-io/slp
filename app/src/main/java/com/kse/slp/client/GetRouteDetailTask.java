package com.kse.slp.client;

import com.kse.slp.R;
import com.kse.slp.route.Point;
import com.kse.slp.route.PointList;
import com.kse.slp.route.RouteInfo;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Hoàng Văn Khoa
 * @since 01-10-2016.
 */
public class GetRouteDetailTask extends ClientBaseTask<RouteInfo, Void, List<Point>> {

    GetRouteDetailTask(Client client, ResponseListener<List<Point>> listener) {
        super(client, listener);
    }

    @Override
    protected List<Point> doInBackground(RouteInfo... routeInfos) {
        RouteInfo routeInfo = routeInfos[0];
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        String url = context.getString(R.string.url_slp_base) + context.getString(R.string.url_load_route_detail);
        HttpEntity<RouteInfo> request = new HttpEntity<>(routeInfo, requestHeaders);
        Class<Point[]> responseType = Point[].class;

        try {
            List<Point> pointList = new ArrayList<>();
            ResponseEntity<Point[]> response = rest.postForEntity(url, request, responseType);
            Point[] points = response.getBody();
            Collections.addAll(pointList, points);
            return pointList;
        } catch (ResourceAccessException | HttpClientErrorException | HttpServerErrorException e) {
            this.e = e;
            return null;
        }
    }
}
