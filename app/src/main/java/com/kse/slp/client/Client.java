package com.kse.slp.client;

import android.content.Context;

import com.kse.slp.R;
import com.kse.slp.auth.AuthenticationInfo;
import com.kse.slp.route.Point;
import com.kse.slp.route.RouteInfo;
import com.kse.slp.ship.DeliveredStatus;
import com.kse.slp.ship.LocationInfo;

import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.util.List;

/**
 * @author Hoàng Văn Khoa
 * @since 30-09-2016.
 */
public class Client {

    private Context context;

    private RestTemplate rest;

    public Client(Context context) {
        this.context = context;

        rest = new RestTemplate();
        rest.getMessageConverters().add(new GsonHttpMessageConverter());
    }

    public void login(AuthenticationInfo authInfo, ResponseListener<ResultInfo> responseListener) {
        new AuthenticationTask(this, authInfo, responseListener).execute();
    }

    public void getRoute(ResponseListener<List<RouteInfo>> listener) {
        new GetRouteTask(this, listener).execute();
    }

    public void getRouteDetail(RouteInfo routeInfo, ResponseListener<List<Point>> listener) {
        new GetRouteDetailTask(this, listener).execute(routeInfo);
    }

    public void updateLocation(LocationInfo location, ResponseListener<ResultInfo> listener) {
        new UpdateLocationTask(this, listener).execute(location);
    }

    public void updateDeliveredStatus(DeliveredStatus status, ResponseListener<Void> listener) {
        new UpdateDeliveredStatusTask(this, listener).execute(status);
    }

    public boolean isAuthenticated() {
        CookieStore store = getCookieManager().getCookieStore();
        for (HttpCookie cookie : store.getCookies()) {
            if (context.getString(R.string.pref_jsession_id).equals(cookie.getName())) return true;
        }
        return false;
    }

    public CookieManager getCookieManager() {
        if (CookieManager.getDefault() == null) {
            CookieHandler.setDefault(new CookieManager());
        }
        return (CookieManager) CookieManager.getDefault();
    }

    public void cancel() {
        //TODO implement this
    }

    public Context getContext() {
        return context;
    }

    public RestTemplate getRest() {
        return rest;
    }

}
